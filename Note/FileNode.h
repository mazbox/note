//
//  FileNode.h
//  Note
//
//  Created by Marek Bereza on 13/03/2013.
//  Copyright (c) 2013 Marek Bereza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileNode : NSObject

@property (copy) NSString *path;
@property (readonly, copy) NSMutableArray *children;

- (id)initWithPath:(NSString*) path;
-(void) addChild:(FileNode *)f;
@end
