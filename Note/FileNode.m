//
//  FileNode.m
//  Note
//
//  Created by Marek Bereza on 13/03/2013.
//  Copyright (c) 2013 Marek Bereza. All rights reserved.
//

#import "FileNode.h"

@implementation FileNode

- (id)init {
	return [self initWithPath:NSHomeDirectory()];
}

- (id)initWithPath:(NSString*) path {
	self = [super init];
	if(self) {
		_path = [path copy];
		_children = [[NSMutableArray alloc] init];
		NSError *err;
		
		NSArray *dir = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:_path error:&err];
		for(NSString *p in dir) {
			
			if([p characterAtIndex:0]!='.') {
				
				NSLog(@"%@", p);
				NSString *withSlash = [NSString stringWithFormat:@"%@/%@", _path, p];
				[self.children addObject:[[FileNode alloc] initWithPath:withSlash]];
			}
		}
	}
	return self;
}

-(void) addChild:(FileNode *)f {
	[_children addObject:f];
	
}
@end
