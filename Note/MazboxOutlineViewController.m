//
//  MazboxOutlineViewController.m
//  Note
//
//  Created by Marek Bereza on 13/03/2013.
//  Copyright (c) 2013 Marek Bereza. All rights reserved.
//

#import "MazboxOutlineViewController.h"

@implementation MazboxOutlineViewController
- (id) init {
	self = [super init];
	if(self) {
		_fileNodes = [[NSMutableArray alloc] init];
		NSString *path = NSHomeDirectory();
		path = [path stringByAppendingString: @"/Dropbox/PlainText"];
		_root = [[FileNode alloc] initWithPath:path];
		[_fileNodes addObject:_root];
		
		
	}
	return self;
}
- (void)textDidChange:(NSNotification *)aNotification {
	NSError *err = nil;
	FileNode *node = [outline itemAtRow:[outline selectedRow]];

	[[textbox string] writeToFile:[node path] atomically:NO encoding:NSUTF8StringEncoding error:&err];
	[textbox setTextColor:[NSColor whiteColor]];
	if (err) {
        NSLog(@"Can't do the success thing as we have an error %@", [err localizedDescription] );
    }
	//NSLog(@"HERE: %@", [textbox string]);
}
- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {
	return !item ? [self.fileNodes count]:[[item children] count];
}


- (BOOL) outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {
	BOOL expandable = !item ? YES : [[item children] count]>0;
	return expandable;
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {
	
	return !item ? [self.fileNodes objectAtIndex:index] : [[item children] objectAtIndex:index];
}

BOOL inited = NO;

-(id) outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item {
	//if([[tableColumn identifier] isEqualToString:@"file"]) {
	if(item==_root) [outline expandItem:_root];
		NSString *filename = [
							  [item path]
							  lastPathComponent];
	if([filename isEqualToString:@"Todo.txt"] && !inited) {
		inited = YES;
		NSInteger itemIndex = [outline rowForItem:item];
		if(itemIndex>0) {
			[outline selectRowIndexes: [NSIndexSet indexSetWithIndex: itemIndex] byExtendingSelection:NO];
		}
	}
	return filename;

}

-(void)outlineViewSelectionDidChange:(NSNotification *)notification
{
	FileNode *node = [outline itemAtRow:[outline selectedRow]];
	
	
	BOOL isDir;
	if([[NSFileManager defaultManager] fileExistsAtPath:[node path] isDirectory:&isDir]) {
		if(!isDir) {
			
			NSError *err = nil;
			NSString *data = [NSString stringWithContentsOfFile:[node path] encoding:NSUTF8StringEncoding error: &err];
			[textbox setString:data];
			[[textbox textStorage] setFont:[NSFont fontWithName:@"Menlo" size:11]];
			[textbox setTextColor:[NSColor whiteColor]];
		}
	}
}

@end

