//
//  MazboxAppDelegate.h
//  Note
//
//  Created by Marek Bereza on 13/03/2013.
//  Copyright (c) 2013 Marek Bereza. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MazboxAppDelegate : NSObject <NSApplicationDelegate> {

}
@property (assign) IBOutlet NSWindow *window;

@end
