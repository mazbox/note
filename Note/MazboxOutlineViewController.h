//
//  MazboxOutlineViewController.h
//  Note
//
//  Created by Marek Bereza on 13/03/2013.
//  Copyright (c) 2013 Marek Bereza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileNode.h"

@interface MazboxOutlineViewController : NSObject<NSOutlineViewDataSource, NSOutlineViewDelegate,NSTextViewDelegate> {
	IBOutlet NSTextView *textbox;
	IBOutlet NSOutlineView *outline;
}

@property (copy) NSMutableArray *fileNodes;
@property (copy) FileNode *root;


@end
