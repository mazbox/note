//
//  main.m
//  Note
//
//  Created by Marek Bereza on 13/03/2013.
//  Copyright (c) 2013 Marek Bereza. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	return NSApplicationMain(argc, (const char **)argv);
}
